dotfiles
=========

Contains the config dotfiles for all my linux distros and applications

#### Pulled in from Antix and need to be tests
* slim.conf
* tint2rc

Window Managers
---------------
Openbox

Distros
--------
Arch
Debian


Notes
-----
### For Git
git add .<br>
git status<br>
git commit -m "updated using git"<br>
git push<br>


Machines
----------
Desktop_PC_ONE_OLDER - Antix
Desktop_PC_TWO - Windows 10
LENOVO - LinuxMint 19.3 - Out of Date
HEADLESS_PC - Arch with Neon 
INTEL - LinuxMint 19.3 - Out of Date


List of Live Distros - 2020/2021 ** Outdated
--------------------

#### Live USBs

* **Antix 19.3** Modified
* Lubuntu 20.04 LTS Focal Fossa

#### Live CD's
* Antix 19.3 LiveDVD
* Arch 2021 LiveDVD
* Ubuntu 21.04 LiveDVD
* PeppermintOS 10 LiveDVD w/UB 18.04LTS
* Ubuntu 10.10 LiveCD
* WattOS R5 LiveCD
